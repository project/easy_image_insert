if (Drupal.jsEnabled) {  
	$(document).ready(function() {  		
		
		$('.easy_image_insert a').bind('click', function() {
			id_image = $(this).parents('div.imagefield').attr('id');
						
			image = $('#' + id_image + ' .imagefield-edit-preview').children('img');			
			image_name = $('#' + id_image + ' span.filename').html();		
			image_alt = image.attr('alt');
			path = $('#' + id_image + ' .path').html();			
			preset = $('#' + id_image + ' .easy_image_insert_select_imagecache').val();			
			if(preset != '') {
				image_src = '/' + path + '/imagecache/' + preset + '/' + path + '/' + image_name;
			}
			else {
				image_src = '/' + path + '/' + image_name;
			}
			image_mce = '<img src="' + image_src  + '" mce_src="' + image_src + '" alt="' + image_alt + '" title="' + image_alt + '" />';	
			
			textarea = $('#' + id_image + ' .easy_image_insert_select_textarea').val();
						
			tinyMCE.getInstanceById(textarea).execCommand('mceInsertContent',false,image_mce);
			return false;
	 	})
    });
}

